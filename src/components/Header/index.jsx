import React from "react";
import { NavLink } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  withStyles,
} from "@material-ui/core";
import LocalMoviesIcon from "@material-ui/icons/LocalMovies";
import HomeIcon from '@material-ui/icons/Home';
import { Fragment } from "react";
import { styles } from "./style";
import { connect, useSelector } from "react-redux";

const Header = (props) => {
  const { title, navLink, activeNavLink } = props.classes;

  const me = useSelector((state) => {
    return state.user;
  });

  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton edge="start" color="inherit" aria-label="menu">
          <LocalMoviesIcon />
        </IconButton>
        <Typography variant="h6" className={title}>
          Movie App
        </Typography>

        <NavLink
          activeClassName={activeNavLink}
          className={navLink}
          exact
          to="/"
        >
          <HomeIcon fontSize="large" />
        </NavLink>
        {me ? (
          <Fragment>
            <NavLink
              activeClassName={activeNavLink}
              className={navLink}
              to="/profile"
            >
              Hello, {me.hoTen}
            </NavLink>
          </Fragment>
        ) : (
          <Fragment>
            <NavLink
              activeClassName={activeNavLink}
              className={navLink}
              to="/signin"
            >
              Sign in
            </NavLink>

            <NavLink
              activeClassName={activeNavLink}
              className={navLink}
              to="/signup"
            >
              Sign up
            </NavLink>
          </Fragment>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default connect()(withStyles(styles)(Header));
