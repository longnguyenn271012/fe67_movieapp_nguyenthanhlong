import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
  withStyles,
} from "@material-ui/core";
import React from "react";
import { styles } from "./style";
import { NavLink } from "react-router-dom";

const MovieItem = (props) => {
  const { maPhim, tenPhim, hinhAnh, moTa, ngayKhoiChieu, danhGia } = props.item;
  const { root, media, title, description, premiereTime, rating, button } =
    props.classes;

 // const dispatch = useDispatch();

  return (
    <Card className={root}>
      <CardActionArea>
        <CardMedia className={media} image={hinhAnh} />
        <CardContent>
          <Typography
            className={title}
            gutterBottom
            variant="h6"
            component="h3"
          >
            {tenPhim.substr(0,20) + "..."}
          </Typography>
          <Typography
            className={description}
            variant="subtitle1"
            color="textSecondary"
            component="p"
          >
            {moTa.substr(0, 26) + "..."}
          </Typography>
          <Typography
            className={premiereTime}
            gutterBottom
            variant="body1"
            component="span"
          >
            {ngayKhoiChieu.substr(0, 10)}
          </Typography>

          <Typography
            className={rating}
            gutterBottom
            variant="body1"
            component="span"
            mx="auto"
          >
            {"Rating " + danhGia + "/10"}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <NavLink className={button} to={`/detail/${maPhim}`}>
          <Button orientation="vertical" size="medium" color="primary">
            Learn More
          </Button>
        </NavLink>
      </CardActions>
    </Card>
  );
};

export default withStyles(styles)(MovieItem);
