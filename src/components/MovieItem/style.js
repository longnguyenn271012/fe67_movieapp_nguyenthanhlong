export const styles = () => {
  return {
    root: {
      maxWidth: 280,
    },
    media: {
      height: 180,
    },
    title: {
      textAlign: "center",
      color: "red",
      fontStyles: "bold",
    },
    description: {
      textAlign: "justify",
    },
    premiereTime: {
      color: "green",
    },
    rating: {
      color: "orange",
      marginLeft: "20",
    },
    button: {
      textDecoration: "none",
    },
  };
};
