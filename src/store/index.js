import { combineReducers, createStore, applyMiddleware, compose } from "redux";
import movie from "./reducers/movie";
import user from "./reducers/user";
import thunk from "redux-thunk";

const reducer = combineReducers({
  movie,
  user,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(reducer, composeEnhancers(applyMiddleware(thunk)));
