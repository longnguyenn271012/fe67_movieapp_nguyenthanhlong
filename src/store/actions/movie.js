import { createAction } from ".";
import { actionType } from "./type";
import axios from "axios";

export const fetchMovies = (dispatch, pageNumber) => {
  axios({
    method: "GET",
    url: `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhimPhanTrang?maNhom=GP01&soTrang=${pageNumber}&soPhanTuTrenTrang=12`,
  })
    .then((res) => {
      dispatch(createAction(actionType.SET_MOVIES, res.data.items));
    })
    .catch((err) => {
      console.error(err);
    });
};

export const fetchMovie = (dispatch, maPhim) => {
  axios({
    method: "GET",
    url: `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`,
  })
    .then((res) => {
      dispatch(createAction(actionType.SET_MOVIE_DETAIL, res.data));
    })
    .catch((err) => {
      console.error(err);
    });
};
