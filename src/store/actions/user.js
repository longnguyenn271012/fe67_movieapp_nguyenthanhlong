import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";

export const signUp = (newUser) => {
  axios({
    method: "POST",
    url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy",
    data: newUser,
  })
    .then((res) => {
      alert("Đã đăng kí thành công tài khoản: " + res.data.taiKhoan);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const signIn = (dispatch, user) => {
  axios({
    method: "POST",
    url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
    data: user,
  })
    .then((res) => {
      console.log(res);
      dispatch(createAction(actionType.SET_ME, res.data));

      //localStorage.setItem("t", res.data.accessToken);
      localStorage.setItem("tk", res.data.taiKhoan);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const fetchMe = async (dispatch, taiKhoan) => {
  try {
    const res = await axios({
      method: "POST",
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
      data: { taiKhoan },
    });

    dispatch(createAction(actionType.SET_ME, res.data));
  } catch (err) {
    console.log(err);
  }
};
