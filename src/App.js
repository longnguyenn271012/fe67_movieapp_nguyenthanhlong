import React, { useEffect } from "react";
import Home from "./views/Home";
import Detail from "./views/Detail";
import SignIn from "./views/SignIn";
import SignUp from "./views/SignUp";
import Profile from "./views/Profile";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { fetchMe } from "./store/actions/user";
import { useDispatch } from "react-redux";

const App = () => {
  const dispatch = useDispatch();
  const taiKhoan = localStorage.getItem("tk");

  useEffect(() => {
    // API cho lấy TTTK bằng taiKhoan , ko bằng accessToken

    // const token = localStorage.getItem("t");
    // if (token) fetchMe(dispatch, token);

    if (taiKhoan) fetchMe(dispatch, taiKhoan);
  }, [dispatch]);

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/detail/:maPhim" component={Detail} />
        <Route path="/signin" component={SignIn} />
        <Route path="/signup" component={SignUp}  />
        <Route path="/profile" component={Profile} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
