import { Button, Container, Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Header from "../../components/Header";
import MovieItem from "../../components/MovieItem";
import { useDispatch, useSelector } from "react-redux";
import { fetchMovies } from "../../store/actions/movie";
import { NotInterested, SkipNext, SkipPrevious } from "@material-ui/icons";

const Home = (props) => {
  const [pageNumber, setPageNumber] = useState(1);

  const movieList = useSelector((state) => {
    return state.movie.movieList;
  });

  const me = useSelector((state) => state.user);

  const dispatch = useDispatch();

  useEffect(() => {
    if (me) {
      fetchMovies(dispatch, pageNumber);
    } else {
      props.history.push("/signin");
    }
  }, [dispatch, pageNumber]);

  return (
    <div>
      <Header></Header>

      <Typography component="h1" variant="h4" align="center">
        DANH SÁCH PHIM
      </Typography>

      <Container maxWidth="md">
        <Grid container>
          {movieList.map((item) => {
            return (
              <Grid xs={12} sm={6} md={4} item key={item.maPhim}>
                <MovieItem item={item}></MovieItem>
              </Grid>
            );
          })}
        </Grid>
      </Container>

      <Container maxWidth="xs">
        <Typography component="h6" variant="h6" align="center">
          Trang
        </Typography>
        <Grid container align="center">
          <Grid xs={4}>
            {pageNumber === 1 ? (
              <Button disabled>
                <NotInterested />
              </Button>
            ) : (
              <Button onClick={() => setPageNumber(pageNumber - 1)}>
                <SkipPrevious />
              </Button>
            )}
          </Grid>
          <Grid xs={4}>
            <Button>{pageNumber}</Button>
          </Grid>
          <Grid xs={4}>
            <Button onClick={() => setPageNumber(pageNumber + 1)}>
              <SkipNext />
            </Button>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Home;
