import {
  Button,
  TextField,
  Typography,
  Avatar,
  Container,
  withStyles,
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import React, { useEffect } from "react";
import Header from "../../components/Header";
import * as yup from "yup";
import { useFormik } from "formik";
import { signUp } from "../../store/actions/user";
import { styles } from "./../SignIn/style";
import { connect, useSelector } from "react-redux";

const schema = yup.object().shape({
  taiKhoan: yup.string().required("Bạn không được bỏ trống ô này !"),
  matKhau: yup.string().required("Bạn không được bỏ trống ô này !"),

  hoTen: yup.string().required("Bạn không được bỏ trống ô này !"),
  soDt: yup
    .string()
    .required("Bạn không được bỏ trống ô này !")
    .matches(/^[0-9]+$/g, "Số điện thoại không hợp lệ !"),
  email: yup
    .string()
    .required("Bạn không được bỏ trống ô này !")
    .email("Email không hợp lệ !"),
});

const SignUp = (props) => {
  const { paper, avatar, form, submit } = props.classes;

  const me = useSelector((state) => state.user);

  useEffect(() => {
    if (me) {
      props.history.push("/");
    }
  });
  const {
    touched,
    errors,
    isValid,
    values,
    handleChange,
    handleBlur,
    setTouched,
  } = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      hoTen: "",
      soDt: "",
      email: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    setTouched({
      taiKhoan: true,
      matKhau: true,
      hoTen: true,
      soDt: true,
      email: true,
    });

    if (!isValid) return;

    const newUser = {
      ...values,
      maNhom: "GP01",
      maLoaiNguoiDung: "KhachHang",
    };

    signUp(newUser);
  };

  return (
    <div>
      <Header />

      <Container component="main" maxWidth="sm">
        <div className={paper}>
          <Avatar className={avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Thông Tin Đăng Kí
          </Typography>
          <form className={form}>
            <TextField
              id="taiKhoan"
              label="Tài khoản"
              value={values.taiKhoan}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.taiKhoan && (
              <Typography variant="body2" color="secondary">
                {errors.taiKhoan}
              </Typography>
            )}
            <TextField
              id="matKhau"
              label="Mật khẩu"
              value={values.matKhau}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.matKhau && (
              <Typography variant="body2" color="secondary">
                {errors.matKhau}
              </Typography>
            )}
            <TextField
              id="hoTen"
              label="Họ Tên"
              value={values.hoTen}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.hoTen && (
              <Typography variant="body2" color="secondary">
                {errors.hoTen}
              </Typography>
            )}
            <TextField
              id="soDt"
              label="Số điện thoại"
              value={values.soDt}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.soDt && (
              <Typography variant="body2" color="secondary">
                {errors.soDt}
              </Typography>
            )}
            <TextField
              id="email"
              label="Email"
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.email && (
              <Typography variant="body2" color="secondary">
                {errors.email}
              </Typography>
            )}

            <Button
              autoFocus
              onClick={handleSubmit}
              color="primary"
              variant="contained"
              fullWidth
            >
              Đăng Kí
            </Button>
          </form>
        </div>
      </Container>
    </div>
  );
};

export default connect()(withStyles(styles)(SignUp));
