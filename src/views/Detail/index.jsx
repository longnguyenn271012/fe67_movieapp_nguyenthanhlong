import React, { useEffect } from "react";
import Header from "../../components/Header";
import { useDispatch, useSelector } from "react-redux";
import { fetchMovie } from "../../store/actions/movie";
import { CardMedia, Container, Grid, Paper } from "@material-ui/core";

const Detail = (props) => {
  const movieDetail = useSelector((state) => {
    return state.movie.movieDetail;
  });

  const { tenPhim, hinhAnh, moTa, trailer } = movieDetail || {};

  const dispatch = useDispatch();

  useEffect(() => {
    fetchMovie(dispatch, props.match.params.maPhim);
  }, [dispatch]);

  return (
    <div>
      <Header />

      <Container maxWidth="md">
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <img src={hinhAnh} alt="hinhAnh" style={{ width: "100%" }} />
          </Grid>
          <Grid item xs={8}>
            <Grid item xs={12}>
              <h1>{tenPhim}</h1>
              <p>{moTa}</p>
            </Grid>
            <Grid item xs={12}>
              <iframe
                src={trailer}
                maxWidth
                title="Trailer"
                style={{ width: "100%", height: 300 }}
              />
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Detail;
