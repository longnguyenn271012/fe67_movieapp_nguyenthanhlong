import React from "react";
import Header from "../../components/Header";
import { connect, useSelector } from "react-redux";
import {
  Avatar,
  Container,
  Table,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
  withStyles,
} from "@material-ui/core";
import { styles } from "./style";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

const Profile = (props) => {
  const me = useSelector((state) => {
    return state.user || {};
  });

  const { paper, table, avatar, title, value } = props.classes;

  return (
    <div>
      <Header />

      <Container component="main" maxWidth="sm">
        <div className={paper}>
          <Typography
            component="h1"
            variant="h4"
            color="secondary"
            fontStyle="bold"
          >
            Thông Tin Tài Khoản
          </Typography>
          <Avatar className={avatar}>
            <AccountCircleIcon fontSize="large" />
          </Avatar>
          <TableContainer>
            <Table className={table}>
              <TableRow>
                <TableCell className={title}>Tài Khoản</TableCell>
                <TableCell className={value}>{me.taiKhoan}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell className={title}>Họ tên</TableCell>
                <TableCell className={value}>{me.hoTen}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell className={title}>Email</TableCell>
                <TableCell className={value}>{me.email}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell className={title}>Số điện thoại</TableCell>
                <TableCell className={value}>{me.soDT}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell className={title}>Loại người dùng</TableCell>
                <TableCell className={value}>{me.maLoaiNguoiDung}</TableCell>
              </TableRow>
            </Table>
          </TableContainer>
        </div>
      </Container>
    </div>
  );
};

export default connect()(withStyles(styles)(Profile));
