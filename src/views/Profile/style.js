export const styles = (theme) => {
  return {
    paper: {
      marginTop: theme.spacing(8),
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.primary.dark,
    },
    table: {
      minWidth: 400,
    },
    title: {
        color: "black",
        fontSize: 20,
        fontStyle: "bold",
    },
    value: {
        color: theme.palette.primary.main,
        fontSize: 20,
        fontStyle: "bold",
    },
  };
};
